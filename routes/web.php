<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CarController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

Route::get('/users', [AdminController::class, 'index'])->middleware('admin');

Route::delete('/deleteuser/{user}',[AdminController::class, 'deleteUser'])->middleware('admin');

Route::get('/mycars', [CarController::class, 'mycars'])->name('mycars');

Route::delete('/destroypicture/{carphoto}',[CarController::class, 'destroyPicture'])->name('destroypicture');

Route::post('/updatecar/{car}', [CarController::class, 'updateCar'])->name('updatecar');

Route::resource('cars', CarController::class);
